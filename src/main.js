import React, { useState } from "react"
import { AuthProvider } from "react-auth-kit"
import { ThemeContext } from "styled-components"
import Routes from "./page/routes"
import { Detector } from "react-detect-offline";
import { toast, ToastContainer } from "react-toastify";

export const RootMain = () => {
    const [isPopup, setIsPopup] = useState({
        show: false,
        type: "",
        title: null,
        content: "",
        fullscreen: false,
        size: "lg",
        dialogClassName: "",
        centered: true,
        height: "auto",
        button: null
    })

    let data = {
        popup: { isPopup, setIsPopup }
    }
    return <ThemeContext.Provider value={data}>
        <React.StrictMode>
            <AuthProvider authType={'cookie'}
                authName={'_auth'}
                cookieDomain={window.location.hostname}
                cookieSecure={window.location.protocol === "https:"}>
                <Routes></Routes>
            </AuthProvider>
        </React.StrictMode>
        <ToastContainer
            position="top-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
        <Detector
            render={({ online }) => !online && toast.error("your connection is offline, please refresh your browser!")}
        />

    </ThemeContext.Provider>
}