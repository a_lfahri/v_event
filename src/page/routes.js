import React from 'react';
import { BrowserRouter as Router, Redirect } from 'react-router-dom';
import { Switch, Route } from 'react-router-loading';
import { NotFound } from './notfound';
import { LoginPage } from './website/login';
import { PrivateRoute } from 'react-auth-kit'
import { Lobby } from './website/items/lobby';
import { Profile } from './website/items/profile';
import { NavbarItems } from './website/component/navbar'
import { FooterComponent } from './website/component/footer';
import { MainContent } from './styles';


export default function Routes() {
    const urlWeb = process.env.REACT_APP_API_ROOT
    
    return (
        <Router>
            <Switch maxLoadingTime={1000}>
                <Route path="/" component={LoginPage} exact />
                <PrivateRoute path={urlWeb} loginPath={'/'} loading render={({ match: { url } }) => (
                    <MainContent className="d-flex justify-content-between min-vh-100 position-relative flex-column">
                        <NavbarItems />
                        <div className="mb-auto d-flex flex-column flex-1" id="main-content">
                            <Switch>
                                <Route path={`${url}`} render={() => {
                                    return <Redirect to={`${url}/lobby`} />
                                }} exact /> 
                                <Route path={`${url}/lobby`} render={() => <Lobby background={"https://cutewallpaper.org/21/wallpapers-keren/Wallpaper-Black-Download-free-beautiful-full-HD-.jpg"} title="" />} exact />
                                <Route path={`${url}/profile`} component={Profile} exact />
                                <Route path={`*`} component={NotFound} exact></Route>
                            </Switch>
                        </div>
                        <FooterComponent />
                    </MainContent>
                )} />
                <Route path="*" component={NotFound}></Route>
            </Switch>
        </Router>
    );
}