import { useContext } from "react";
import { Button } from "react-bootstrap"
import { ThemeContext } from "styled-components";
import { _handlers } from "../../actions/handlers"
import { ModalComponent } from "../../component/dialog"
export const Lobby = (props) => {
  const theme = useContext(ThemeContext);
  const { popup } = theme
  const { background } = props
  return <div className="flex-1 p-4" style={{ backgroundImage: `url(${background})`, backgroundRepeat: "no-repeat", backgroundSize: "cover" }}>
    <Button onClick={() => _handlers("video", popup)}>
      Video
    </Button>
    <Button onClick={() => _handlers("sliderImage", popup)}>
      Owl Crausel
    </Button>
    <Button onClick={() => _handlers("sliderVideo", popup)}>
      Owl Crausel video
    </Button>
    <Button onClick={() => _handlers(false, popup)}>
      Help Desk
    </Button>
    <Button>
      Agenda
    </Button>
    <Button>
      Tutorial
    </Button>
    <ModalComponent></ModalComponent>
  </div>
}