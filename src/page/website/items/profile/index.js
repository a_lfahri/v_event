export const Profile = () => {
    return <div className="flex-1 p-4 d-flex flex-column">
        <div className="row flex-1">
            <div className="col-12 col-lg-8 col-xl-9 d-flex flex-column mb-4 mb-lg-0">
                <div className="card flex-1 shadow border-0">
                    <div className="card-header">
                        <h4 className="card-title">Bookmark</h4>
                    </div>
                    <div className="card-body">

                    </div>
                </div>
            </div>
            <div className="col d-flex flex-column">
                <div className="card flex-1 shadow border-0">
                    <div className="card-body p-0">
                        <div className="justify-content-center d-flex py-4 border-bottom">
                            <div className="d-flex align-items-center">
                                <div className="border rounded-circle">
                                    <img className="rounded-circle" height="90" width="90" alt="img" src="https://www.its.ac.id/it/wp-content/uploads/sites/46/2021/06/blank-profile-picture-973460_1280-300x300.png"></img>
                                </div>
                                <div className="ps-4">
                                    <p className="m-0">Harris Munahar</p>
                                    <p className="m-0">zimibot@gmail.com</p>
                                    <form className="position-relative overflow-hidden">
                                        <div className="form-group pointer-event">
                                            <button className="color-primary small bg-transparent border-0">
                                                <input type="file" className=" w-100 opacity-0 position-absolute pointer-event" style={{ left: 0 }} />
                                                Upload Foto</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="px-5 py-4">
                            <div className="mb-3">
                                <h5>Job Title</h5>
                                <span>Tester</span>
                            </div>
                            <div className="mb-3">
                                <h5>Description</h5>
                                <span>Tester</span>
                            </div>
                            <div className="mb-3">
                                <h5>Job Title</h5>
                                <span>Tester</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}