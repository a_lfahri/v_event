import { ImagesSlider } from "../../component/popup/images";
import { TabView } from "../../component/popup/tab-view";
import { VideoPopup, VideoSlider } from "../../component/popup/video";

export const _handlers = (type, handler) => {
    // eslint-disable-next-line no-lone-blocks
    /* `config popup`
        show: false,
        type: "",
        title: null,
        content: "",
        fullscreen: false,
        size: "lg",
        dialogClassName: "",
        centered: true,
        height: "auto",
        button: null
    */
    const { setIsPopup } = handler
    switch (type) {
        case "video":
            setIsPopup(d => ({
                ...d,
                content: <VideoPopup/>,
                height: "500px", 
                dialogClassName: "no-padding full-video close-absolute",
                size: "xl",
                show: true
            }))
            break;
        case "sliderImage":
            setIsPopup(d => ({
                ...d,
                content: <ImagesSlider/>,
                dialogClassName: "no-padding close-absolute bg-black-transparant",
                size: "lg",
                show: true
            }))
            break
        case "sliderVideo":
            setIsPopup(d => ({
                ...d,
                content: <VideoSlider/>,
                dialogClassName: "no-padding close-absolute arrow-black",
                size: "lg",
                show: true
            }))
            break
        default:
            setIsPopup(d => ({
                ...d,
                content: <TabView/>,
                dialogClassName: "no-padding border-none",
                size: "lg",
                title:"Help Desk",
                show: true
            }))
            break;
    }


}