import { Container } from "./style"
import { useForm } from "react-hook-form";
import { FaFacebook, FaInstagram, FaLinkedin, FaLock, FaTwitter, FaUser } from 'react-icons/fa';
import { toast } from 'react-toastify';
import { useSignIn } from 'react-auth-kit'
import { useIsAuthenticated } from 'react-auth-kit';
import { Redirect } from 'react-router-dom';
import axios from "axios";


export const LoginPage = (props) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const signIn = useSignIn()
    const isAuthenticated = useIsAuthenticated()
    const url = process.env.REACT_APP_API_ROOT
    const api = process.env.REACT_APP_API_URL
    const onSubmit = data => {
        axios.post(`${api}/api/signin`, data).then((v) => {
            let { data } = v
            if (data.code === 403 || data.code === 404) {
                toast.warn(data.message, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }  else if(data.code === 200) {
                signIn({
                    token: data.token, tokenType: "Bearer", expiresIn: 200, authState: {
                        data: data.data
                    }
                })
                props.history.push(url);
            } else {
                toast.warn("Api Error", {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        })
    };
    if (isAuthenticated()) {
        return <Redirect to={url}></Redirect>
    }
    return (
        <Container className="container">
            <div className="container__forms">
                <div className="form">
                    <form className="form__sign-in" onSubmit={handleSubmit(onSubmit)}>
                        <h2 className="form__title text-white">Sign In</h2>
                        <div className="form__input-field">
                            <span className="icon-custom">
                                <FaUser />
                            </span>
                            <input type="email" placeholder="Email"  {...register("email", { required: true })} />
                        </div>
                        {errors.email && <span className="text-danger">This field Email is required</span>}
                        <div className="form__input-field">
                            <span className="icon-custom">
                                <FaLock />
                            </span>
                            <input type="password" placeholder="Password"  {...register("password", { required: true })} />
                        </div>
                        {errors.password && <span className="text-danger">This field Password is required</span>}
                        <button className="form__submit" type="submit" >
                            Submit
                        </button>
                        <p className="form__social-text text-white">Social platforms</p>
                        <div className="form__social-media">
                            <a href="/" className="form__social-icons text-white border-white">
                                <FaFacebook />
                            </a>
                            <a href="/" className="form__social-icons text-white border-white">
                                <FaTwitter />
                            </a>
                            <a href="/" className="form__social-icons text-white border-white">
                                <FaInstagram />
                            </a>
                            <a href="/" className="form__social-icons text-white border-white">
                                <FaLinkedin />
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <div className="container__panels">
                <div className="panel panel__left">
                    <div className="panel__content">
                        <h3 className="panel__title">New here ?</h3>
                        <p className="panel__paragraph">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Doloremque adipisci tempore aliquid?
                        </p>
                        <button className="btn btn-transparent" id="sign-up-btn">
                            Sign Up
                        </button>
                    </div>
                    <img className="panel__image" src="https://stories.freepiklabs.com/storage/11588/market-launch-amico-2628.png" alt="" />
                </div>
                <div className="panel panel__right">
                    <div className="panel__content">
                        <h3 className="panel__title">One of us ?</h3>
                        <p className="panel__paragraph">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Doloremque adipisci tempore aliquid?
                        </p>
                        <button className="btn btn-transparent" id="sign-in-btn">
                            Sign In
                        </button>
                    </div>
                    <img className="panel__image" src="https://www.pngkey.com/png/full/444-4444270_ia-press-play-website.png" alt="" />
                </div>
            </div>
          
        </Container>

    )
}