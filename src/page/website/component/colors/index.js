export const Colors = {
    default: "#b538d1",
    seconds: "#d672ec",
    border: "#333",
    text1: "#fff",
    text2: "#aaa",
    input: "#ccc"
}