export const FooterComponent = () => {
    return (
        <div className="d-flex justify-content-between align-items-center px-4 py-2 w-100 bg-white shadow border-top">
            <div>
                &#169; Website
            </div>
            <div>
                <img height="30"  alt="img" src="https://assets.grab.com/wp-content/uploads/media/ir/logo/Grab_Final_Master_Logo_2021_RGB_(green).png"/>
            </div>
        </div>
    )
}