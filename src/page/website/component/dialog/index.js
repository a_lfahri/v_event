import { useContext, useState } from "react"
import { Modal, Button } from "react-bootstrap"
import styled, { ThemeContext } from "styled-components"



const MainModal = styled.div`
.modal-body {
        min-height: ${props => props.height}
    }
`

export const ModalComponent = (props) => {
    const theme = useContext(ThemeContext);
    const { popup } = theme
    const { isPopup, setIsPopup } = popup

    return <Modal
        show={isPopup.show}
        fullscreen={isPopup.fullscreen}
        backdrop="static"
        size={isPopup.size}
        onHide={() => setIsPopup(d => ({
            ...d,
            show: false
        }))}
        keyboard={false}
        dialogClassName={isPopup.dialogClassName}
        centered={isPopup.centered}
    >
        <MainModal height={isPopup.height}>
            <Modal.Header closeButton>
                {isPopup.title &&
                    <Modal.Title className="ms-auto fa-bold">
                        {isPopup.title}
                    </Modal.Title>
                }
            </Modal.Header>
            <Modal.Body>
                {isPopup.content}
            </Modal.Body>
            {isPopup.button &&
                <Modal.Footer>
                    <Button variant="primary">{isPopup.button}</Button>
                </Modal.Footer>
            }
        </MainModal>
    </Modal>
}