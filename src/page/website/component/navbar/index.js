import { Dropdown } from "react-bootstrap"
import { FaArrowDown } from "react-icons/fa"
import { useSignOut } from 'react-auth-kit'
import { useAuthUser } from 'react-auth-kit'
import defaultPicture from '../../../../assets/images/default_user.png'

export const NavbarItems = () => {
    const signOut = useSignOut()
    const auth = useAuthUser()
    const { data } = auth()
    let fullName = `${data.first_name} ${data.last_name}`
    return (
        <nav className="navbar navbar-expand bg-white shadow border-bottom position-relative">
            <div className="nav navbar-nav justify-content-between d-flex w-100 align-items-center pe-4">
                <div className="d-flex align-items-center">
                    <div className="py-2 px-4">
                        <a href="/">
                            <img alt="img" height="35" src="https://assets.grab.com/wp-content/uploads/media/ir/logo/Grab_Final_Master_Logo_2021_RGB_(green).png" />
                        </a>
                    </div>
                    <a href="/" className="text-black px-4 py-2 active">
                        Lobby
                    </a>
                    <a href="/" className="text-black px-4 py-2">
                        Poster
                    </a>
                    <a href="/" className="text-black px-4 py-2">
                        Exhibition
                    </a>
                    <a href="/" className="text-black px-4 py-2">
                        Booth
                    </a>
                </div>
                <div>
                    <Dropdown>
                        <Dropdown.Toggle className="bg-transparent border-0 d-flex align-items-center" id="dropdown-basic">
                            <div className="text-black pe-3">{fullName}</div>
                            <div>
                                <img className="rounded-circle" alt="img" width="50" height="50" src={data.picture ? data.picture : defaultPicture} />
                            </div>
                            <span className="text-black ps-3">
                                <FaArrowDown />
                            </span>
                        </Dropdown.Toggle>
                        <Dropdown.Menu className="w-100">
                            <Dropdown.Item href="/web/profile">Profile</Dropdown.Item>
                            <Dropdown.Item onClick={() => signOut()}>Log Out</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </nav>
    )
}