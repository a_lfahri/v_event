import ReactPlayer from "react-player"
import { Carousel } from "react-responsive-carousel"


export const VideoPopup = () => {
    return <div>
        <ReactPlayer width="100" height="100" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
    </div>
}


export const VideoSlider = () => {
    return (
        <Carousel thumbWidth={90} swipeable autoPlay autoFocus showThumbs={false} infiniteLoop stopOnHover verticalSwipe="standard">
            <div className="h-100">
                <ReactPlayer width="100" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
               
            </div>
            <div className="h-100">
                <ReactPlayer width="100" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
               
            </div>
            <div className="h-100">
                <ReactPlayer width="100" url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
            </div>
        </Carousel>
    )
}