import { Tab, Tabs } from "react-bootstrap"

export const TabView = () => {
    return (
        <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
            <Tab eventKey="home" title="Home">
                <div className="px-4 pb-3">
                    test-1
                </div>
            </Tab>
            <Tab eventKey="profile" title="Profile">
                <div className="px-4 pb-3">
                    test-1
                </div>
            </Tab>
            <Tab eventKey="contact" title="Contact">
                <div className="px-4 pb-3">
                    test-1
                </div>
            </Tab>
        </Tabs>
    )
}