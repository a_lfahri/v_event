import { Carousel } from 'react-responsive-carousel';

export const ImagesSlider = () => {
    return (
        <Carousel thumbWidth={90} swipeable centerMode autoPlay autoFocus infiniteLoop stopOnHover verticalSwipe="standard">
            <div className="h-100">
                <img alt="img" className="h-100" src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                <p className="legend">Legend 1</p>
            </div>
            <div className="h-100">
                <img alt="img" className="h-100" src="https://www.wikihow.com/images_en/thumb/6/65/Cite-Google-Images-Step-1.jpg/v4-460px-Cite-Google-Images-Step-1.jpg.webp" />
                <p className="legend">Legend 2</p>
            </div>
            <div className="h-100">
                <img alt="img" className="h-100" src="https://cdn.searchenginejournal.com/wp-content/uploads/2019/03/the-10-best-image-search-engines-julia-mccoy-760x400.png" />
                <p className="legend">Legend 3</p>
            </div>
        </Carousel>
    )
}