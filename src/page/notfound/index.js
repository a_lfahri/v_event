import "./style.css"

export const NotFound = () => {
    return (
        <div id='oopss'>
            <div id='error-text'>
                <span>404</span>
                <p>PAGE NOT FOUND</p>
                <p className='hmpg'><a href='/' className="back">Back To Home</a></p>
            </div>
        </div>
    )
}