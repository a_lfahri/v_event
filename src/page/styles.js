import styled from "styled-components";
import { Colors } from "./website/component/colors";


export const MainContent = styled.div `
    a.active {
        border-bottom: 1px solid ${Colors.default};
        color: ${Colors.default}!important;
    }
  
    .border {
        border-color: ${Colors.border}!important;
    }

    .card {
        border-bottom: 2px solid ${Colors.default}!important;
    }
    .color-primary {
        color: ${Colors.default}
    }

    .carousel .thumb.selected, .carousel .thumb:hover {
        border-radius: 8px;
        border-color: ${Colors.default};
    }
    
`